//
//  GameScheme.swift
//  Don't Fall Off The Table
//
//  Created by Brandon Mathis on 8/18/17.
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class GameScheme
    {
    private var color = 0
    private var shape = 0
    private var tableColors = [[UIColor]]()
    private var colorLabels = [String]()
    private var shapeLabels = [String]()
    private var wind: Wind!
    private var windChanged = true
    private var previousDirection = -1
    var xWind = 0.0
    var yWind = 0.0
    
//=========================================================
//Initialize the game settings
    init(colorIn: Int, shapeIn: Int)
        {
        color = colorIn
        shape = shapeIn
        wind = Wind(size: 0.0, random: 0)
        setColorArrays()
        setShapeLabels()
        }
//=========================================================
//Returns whether labels should be displayed on top half of table
    func hasTopLabel() -> Bool
        {
        return shape != 4 && shape != 2 && shape != 10
        }
//=========================================================
//Returns whether labels should be displayed on bottom half of table
    func hasBottomLabel() -> Bool
        {
        return shape != 3 && shape != 9
        }
//=========================================================
//Initializes an array with the potential table colors
    private func setColorArrays()
        {
        var tableColor = [UIColor]()
        var colorPattern = Float(255.0)
        for _ in 0 ... 4                //Default
            {
            tableColor.append(UIColor(colorLiteralRed: 0/255.0, green: colorPattern/255.0, blue: colorPattern/255.0, alpha: 1.0))
            colorPattern = colorPattern - 25
            }
        tableColors.append(tableColor)
        tableColor.removeAll()
        colorLabels.append("Default")
            
        colorPattern = Float(150)
        for _ in 0 ... 4                    //Red
            {
            tableColor.append(UIColor(colorLiteralRed: colorPattern/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0))
            colorPattern = colorPattern + 25
            }
        tableColors.append(tableColor)
        tableColor.removeAll()
        colorLabels.append("Red")
            
        colorPattern = Float(150)
        for _ in 0 ... 4                //Orange
            {
            tableColor.append(UIColor(colorLiteralRed: colorPattern/255.0, green: (colorPattern/2)/255.0, blue: 0/255.0, alpha: 1.0))
            colorPattern = colorPattern + 25
            }
            tableColors.append(tableColor)
            tableColor.removeAll()
            colorLabels.append("Orange")
            
        colorPattern = Float(150)
        for _ in 0 ... 4                //Yellow
            {
            tableColor.append(UIColor(colorLiteralRed: colorPattern/255.0, green: colorPattern/255.0, blue: 0/255.0, alpha: 1.0))
            colorPattern = colorPattern + 25
            }
            tableColors.append(tableColor)
            tableColor.removeAll()
            colorLabels.append("Yellow")
            
            colorPattern = Float(150)
        for _ in 0 ... 4                //Green
            {
            tableColor.append(UIColor(colorLiteralRed: 0/255.0, green: colorPattern/255.0, blue: 0/255.0, alpha: 1.0))
            colorPattern = colorPattern + 25
            }
        tableColors.append(tableColor)
        tableColor.removeAll()
        colorLabels.append("Green")
            
            colorPattern = Float(150)
        for _ in 0 ... 4                //Blue
            {
            tableColor.append(UIColor(colorLiteralRed: 0/255.0, green: 0/255.0, blue: colorPattern/255.0, alpha: 1.0))
            colorPattern = colorPattern + 25
            }
        tableColors.append(tableColor)
        tableColor.removeAll()
        colorLabels.append("Blue")
            
        colorPattern = Float(150)
        for _ in 0 ... 4                //Purple
            {
            tableColor.append(UIColor(colorLiteralRed: (colorPattern/3)/255.0, green: 0/255.0, blue: colorPattern/255.0, alpha: 1.0))
            colorPattern = colorPattern + 25
            }
        tableColors.append(tableColor)
        tableColor.removeAll()
        colorLabels.append("Purple")
 
                                        //Rainbow
        tableColor.append(UIColor(colorLiteralRed: 0/255.0, green: 0/255.0, blue: 255.0/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 0/255.0, green: 200.0/255.0, blue: 0/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 255.0/255.0, green: 255.0/255.0, blue: 0/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 255.0/255.0, green: 127/255.0, blue: 0/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 255.0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0))
        tableColors.append(tableColor)
        tableColor.removeAll()
        colorLabels.append("Rainbow")

                                        //Camo
        tableColor.append(UIColor(colorLiteralRed: 96/255.0, green: 68/255.0, blue: 57/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 68/255.0, green: 83/255.0, blue: 59/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 96/255.0, green: 68/255.0, blue: 57/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 68/255.0, green: 83/255.0, blue: 59/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 96/255.0, green: 68/255.0, blue: 57/255.0, alpha: 1.0))
        tableColors.append(tableColor)
        tableColor.removeAll()
        colorLabels.append("Camo")
            
                                        //America
        tableColor.append(UIColor(colorLiteralRed: 187/255.0, green: 19/255.0, blue: 62/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 0/255.0, green: 33/255.0, blue: 71/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 187/255.0, green: 19/255.0, blue: 62/255.0, alpha: 1.0))
        tableColors.append(tableColor)
        tableColor.removeAll()
        colorLabels.append("America")
            
                                        //Illusion
        tableColor.append(UIColor(colorLiteralRed: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0))
        tableColor.append(UIColor(colorLiteralRed: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0))
        tableColors.append(tableColor)
        tableColor.removeAll()
        colorLabels.append("Illusion")
        }
//===========================================================
//Initializes an array with the potential table shape names
    private func setShapeLabels()
        {
        shapeLabels.append("Circle")
        shapeLabels.append("Square")
        shapeLabels.append("Triangle")
        shapeLabels.append("Inverted Triangle")
        shapeLabels.append("Heart")
        shapeLabels.append("Letter L")
        shapeLabels.append("Backwards Letter L")
        shapeLabels.append("Letter Z")
        shapeLabels.append("Letter T")
        shapeLabels.append("Top Half Circle")
        shapeLabels.append("Bottom Half Circle")
        }
//==========================================================
//Sets the color and table shape to be used
    func setScheme(colorIn: Int,shapeIn: Int)
        {
        color = colorIn
        shape = shapeIn
        }
//==========================================================
//Returns the list of potential colors
    func getColorList() -> [String]
        {
        return colorLabels
        }
//==========================================================
//Returns the list of potential shapes
    func getShapeList() -> [String]
        {
        return shapeLabels
        }
//==========================================================
//Returns the selected colors
    func getTableColor(index: Int) -> UIColor
        {
        return tableColors[color][index]
        }
//=========================================================
//Returns the Table to be used for the game
    func getTable(radius: CGFloat) -> Table
        {
        if(shape == 0)
            {
            return Table(circleOfRadius: CGFloat(radius))
            }
        else if(shape == 1)
            {
            return Table(squareInRadius: CGFloat(radius))
            }
        else if(shape == 2)
            {
                return Table(triangleInRadius: CGFloat(radius),up: 0)
            }
        else if(shape == 3)
            {
            return Table(triangleInRadius: CGFloat(radius),up: 1)
            }
        else if(shape == 4)
            {
            return Table(heartInRadius: CGFloat(radius))
            }
        else if(shape == 5)
            {
            return Table(lInRadius: CGFloat(radius),up: 0)
            }
        else if(shape == 6)
            {
            return Table(lInRadius: CGFloat(radius),up: 1)
            }
        else if(shape == 7)
            {
            return Table(zInRadius: CGFloat(radius))
            }
        else if(shape == 8)
            {
            return Table(tInRadius: CGFloat(radius))
            }
        else if(shape == 9)
            {
            return Table(halfCircleInRadius: CGFloat(radius), up: 0)
            }
        else
            {
            return Table(halfCircleInRadius: CGFloat(radius), up: 1)
            }
        }
//==========================================================
//Changes the ball color based on the table color selected
    func getBallColor() -> UIColor
        {
        if(color == 10 || color == 9)
            {
            return UIColor.gray
            }
        else
            {
            return UIColor.black
            }
        }
//=========================================================
//Returns the wind direction
    func getWind() -> Wind
        {
        return wind
        }
//==========================================================
//Returns whether the wind direction changed
    func windDidChange() -> Bool
        {
        return windChanged
        }
//==========================================================
//Gets the value for the wind
    func getWindDirection(boxSize: Double)
        {
        let randomNum = Int(arc4random_uniform(7))
        if(randomNum != previousDirection)
            {
            windChanged = true
            if(randomNum == 0)
                {
                xWind = 0
                yWind = 50
                }
            else if(randomNum == 1)
                {
                xWind = 0
                yWind = -50
                }
            else if(randomNum == 2)
                {
                xWind = 50
                yWind = 0
                }
            else if(randomNum == 3)
                {
                xWind = -50
                yWind = 0
                }
            else if(randomNum == 4)
                {
                xWind = -50
                yWind = -50
                }
            else if(randomNum == 5)
                {
                xWind = 50
                yWind = 50
                }
            else if(randomNum == 6)
                {
                xWind = -50
                yWind = 50
                }
            else
                {
                xWind = 50
                yWind = -50
                }
                xWind = xWind/75
                yWind = yWind/75
            wind.setWind(size:boxSize,random: randomNum)
            previousDirection = randomNum
            }
        else
            {
            windChanged = false
            }
        }
    }
