//
//  TableColorScene.swift
//  Don't Fall Off The Table
//
//  Created by Brandon Mathis on 8/18/17.
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//

import Foundation
import SpriteKit

class TableColorScene: SKScene
{
    var buttons = [SKLabelNode]()
    var returnButton: SKLabelNode!
    var nextButton: SKLabelNode!
    var controller: GameViewController!
    var yPos = CGFloat(0)
    var selectedLabel: SKLabelNode!
    override func didMove(to view: SKView)
        {

        }
//==============================================================================
//Sets up the select screen for table colors
    func setupDisplay(to view: SKView)
        {
        backgroundColor = UIColor(colorLiteralRed: 159.0/255.0, green: 200.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        let backgroundPicture = SKSpriteNode(imageNamed: "table.png")
        backgroundPicture.position = CGPoint(x: frame.midX, y: frame.midY)
        backgroundPicture.scale(to: CGSize(width: frame.maxX + 40, height: frame.maxX + 40))
        backgroundPicture.alpha = 0.2
        addChild(backgroundPicture)
        let colorLabel = SKLabelNode()
        colorLabel.text = "Table Colors"
        colorLabel.fontSize = CGFloat(frame.midX/5)
        colorLabel.fontName = "AvenirNext-Bold"
        colorLabel.fontColor = UIColor.black
        colorLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        colorLabel.position = CGPoint(x: frame.midX, y: frame.maxY)
        addChild(colorLabel)
        yPos = frame.maxY - CGFloat(100)
        var firstPass = true
        let list = controller.gameScheme.getColorList()
        for color in list
            {
            let button = SKLabelNode()
            button.text = color
            button.fontSize = CGFloat(frame.midX/7)
            button.fontName = "AvenirNext-Bold"
            if(controller.hasColors || firstPass)
                {
                button.fontColor = UIColor.black
                firstPass = false
                }
            else
                {
                button.fontColor = UIColor.lightGray
                }
            button.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
            button.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
            button.position = CGPoint(x: 0, y: yPos)
            addChild(button)
            buttons.append(button)
            yPos = yPos - CGFloat(Double(frame.midY)/(Double(list.count)/1.5))
            }
            
        selectedLabel = SKLabelNode()
        selectedLabel.text = "X"
        selectedLabel.fontSize = CGFloat(frame.midX/7)
        selectedLabel.fontName = "AvenirNext-Bold"
        selectedLabel.fontColor = UIColor.black
        selectedLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        selectedLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.right
        selectedLabel.position = CGPoint(x: frame.maxX, y: buttons[controller.colorSelected].position.y)
        addChild(selectedLabel)
        
            
        nextButton = SKLabelNode()
        nextButton.text = "Shapes >> "
        nextButton.fontSize = CGFloat(frame.midX/8)
        nextButton.fontName = "AvenirNext-Bold"
        nextButton.fontColor = UIColor.black
        nextButton.verticalAlignmentMode = SKLabelVerticalAlignmentMode.bottom
        nextButton.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.right
        nextButton.position = CGPoint(x: frame.maxX, y: 5)
        addChild(nextButton)
            
        returnButton = SKLabelNode()
        returnButton.text = "Menu"
        returnButton.fontSize = CGFloat(frame.midX/8)
        returnButton.fontName = "AvenirNext-Bold"
        returnButton.fontColor = UIColor.black
        returnButton.verticalAlignmentMode = SKLabelVerticalAlignmentMode.bottom
        returnButton.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        returnButton.position = CGPoint(x: frame.midX, y: 10)
        addChild(returnButton)
        }
//==============================================================================
//Handles user interaction
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
        {
        if let touch = touches.first
            {
            let pos = touch.location(in: self)
            let node = self.atPoint(pos)
            if(node == nextButton)
                {
                controller.showShapeSettings()
                }
            else if(node == returnButton)
                {
                controller.showMenu()
                }
            else
                {
                var found = false
                var i = 0
                for button in buttons
                    {
                    if(button == node)
                        {
                        found = true
                        if(controller.hasColors)
                            {
                            controller.colorSelected = i
                            selectedLabel.position = CGPoint(x: frame.maxX, y: buttons[controller.colorSelected].position.y)
                            }
                        }
                    i = i + 1
                    }
                if(found && node != buttons.first && !controller.hasColors)
                    {
                    controller.showPurchases()
                    }
                }

            }
        }
    }
