//
//  MenuScene.swift
//  Don't Fall Off The Table
//
//  Created by Brandon Mathis on 8/17/17.
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//
import SpriteKit

class MenuScene: SKScene
    {
    var playButton: SKLabelNode!
    var modeButton: SKLabelNode!
    var purchaseButton: SKLabelNode!
    var controller: GameViewController!

//==============================================================================
//Sets up the menu screen   
    override func didMove(to view: SKView)
        {
        backgroundColor = UIColor(colorLiteralRed: 159.0/255.0, green: 200.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        let backgroundPicture = SKSpriteNode(imageNamed: "table.png")
        backgroundPicture.position = CGPoint(x: frame.midX, y: frame.midY)
        backgroundPicture.scale(to: CGSize(width: frame.maxX + 40, height: frame.maxX + 40))
        backgroundPicture.alpha = 0.2
        addChild(backgroundPicture)
        playButton = SKLabelNode()
        playButton.text = "Play"
        playButton.fontSize = CGFloat(frame.midX/4)
        playButton.fontName = "AvenirNext-Bold"
        playButton.fontColor = UIColor.black
        playButton.position = CGPoint(x: frame.midX, y: frame.maxY * 0.55)
        addChild(playButton)
        modeButton = SKLabelNode()
        modeButton.text = "Game Settings"
        modeButton.fontColor = UIColor.black
        modeButton.fontSize = CGFloat(frame.midX/5)
        modeButton.position = CGPoint(x: frame.midX, y: frame.maxY * 0.45)
        modeButton.fontName = "AvenirNext-Bold"
        addChild(modeButton)
        purchaseButton = SKLabelNode()
        purchaseButton.text = "In-App Purchases"
        purchaseButton.fontColor = UIColor.black
        purchaseButton.fontSize = CGFloat(frame.midX/5)
        purchaseButton.fontName = "AvenirNext-Bold"
        purchaseButton.position = CGPoint(x: frame.midX, y: frame.maxY * 0.35)
        addChild(purchaseButton)
        }
//==============================================================================
//Handles user interaction   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
        {
        if let touch = touches.first
            {
            let pos = touch.location(in: self)
            let node = self.atPoint(pos)
            if (node == playButton)
                {
                controller.startGame()
                }
            else if (node == purchaseButton)
                {
                controller.showPurchases()
                }
            else if(node == modeButton)
                {
                controller.showColorSettings()
                }
            }
        }
    }
