//
//  GameScene.swift
//  Don't Fall Off The Table
//
//  Created by Brandon Mathis on 8/9/17.
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene
    {
    var scoreLabel = SKLabelNode()
    var timeLabel = SKLabelNode()
    var windLabel = SKLabelNode()
    var tables = [Table]()
    var ball:Ball!
    var timer = Timer()
    var timeLeft = 45
    var score = 0
    let scores = [1,5,10,30,50,80]
    var gameOver = false
    var count = 0
    var center = CGPoint(x: 0, y: 0)
    var controller: GameViewController!
    var initialRun = true
    var runCount = 0
    var gameScheme: GameScheme!
    var width: CGFloat!
    var height: CGFloat!
    
    override func didMove(to view: SKView)
        {

        }
//==============================================================================================================
//Sets up the game screen   
    func drawGame(view: SKView)
        {
        var bottomLabels = [SKLabelNode]()
        var topLabels = [SKLabelNode]()
        width = view.frame.size.width/2
        height = view.frame.size.height/2
        var radius = view.frame.size.width/8
        let change = radius
        var scale = CGFloat(1)
        center = CGPoint(x: width, y: height)
        ball = Ball(circleOfRadius: width / 30)
        ball.fillColor = gameScheme.getBallColor()
        ball.strokeColor = ball.fillColor
        backgroundColor = UIColor(colorLiteralRed: 159.0/255.0, green: 200.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        scoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        scoreLabel.position = CGPoint(x: 0, y: height * 2)
        scoreLabel.fontColor = UIColor.black
        scoreLabel.fontSize = CGFloat(frame.midX/7)
        scoreLabel.text = "Score: 0"
        scoreLabel.fontName = "AvenirNext-Bold"
        timeLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.right
        timeLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        timeLabel.position = CGPoint(x: width * 2, y: height * 2)
        timeLabel.fontColor = UIColor.black
        timeLabel.fontName = "AvenirNext-Bold"
        timeLabel.fontSize = CGFloat(frame.midX/7)
        timeLabel.text = "Time: 0:45"
        windLabel = SKLabelNode()
        windLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        windLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        windLabel.position = CGPoint(x: 0, y: height/4)
        windLabel.fontColor = UIColor.black
        windLabel.fontName = "AvenirNext-Bold"
        windLabel.fontSize = CGFloat(frame.midX/7)
        windLabel.text = "Wind: "
        addChild(windLabel)
        gameScheme.getWindDirection(boxSize: Double(width)/5.0)
        let wind = gameScheme.getWind()
        wind.position = CGPoint(x: width - width/2.5, y: windLabel.position.y - width/5)
        ball.addWind(x:gameScheme.xWind/Double(height),y: gameScheme.yWind/Double(height))
        addChild(wind)
        for i in 0 ... 4
            {
            let bottomPosition = CGPoint(x: width, y: height - radius)
            let topPosition = CGPoint(x: width, y: height + radius - 15)
            tables.append(gameScheme.getTable(radius: radius))
            tables[i].position = CGPoint(x: width - radius, y: height - radius)
            tables[i].fillColor = gameScheme.getTableColor(index: i)
            tables[i].strokeColor = ball.fillColor
            bottomLabels.append(SKLabelNode(text: String(scores[i])))
            bottomLabels[i].position = bottomPosition
            bottomLabels[i].fontColor = ball.fillColor
            bottomLabels[i].fontSize = CGFloat(frame.midX/10)
            bottomLabels[i].fontName = "AvenirNext-Bold"
            topLabels.append(SKLabelNode(text: String(scores[i])))
            topLabels[i].position = topPosition
            topLabels[i].fontColor = ball.fillColor
            topLabels[i].fontSize = CGFloat(frame.midX/10)
            topLabels[i].fontName = "AvenirNext-Bold"
            radius = radius + (change * scale)
            scale = scale - 0.2
            }
        for i in 0 ... 4
            {
            addChild(tables[4 - i])
            if(gameScheme.hasTopLabel())
                {
                addChild(topLabels[4 - i])
                }
            if(gameScheme.hasBottomLabel())
                {
                addChild(bottomLabels[4 - i])
                }
            }
        addChild(scoreLabel)
        addChild(timeLabel)
        ball.setDiameter(circleOfRadius: width/20)
        ball.xPos = Double(center.x)
        ball.yPos = Double(center.y)
        ball.position = CGPoint(x:center.x, y: center.y)
        ball.draw()
        addChild(ball)
        if #available(iOS 10.0, *)
            {
            timer = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true, block: updateScore)
            }
        else
            {
            // Fallback on earlier versions
            }
        }
//===============================================================================================================
    override func update(_ currentTime: TimeInterval)
        {
        // Called before each frame is rendered
        }
//===============================================================================================================
//Updates the users score  
    func updateScore(time: Timer)
        {
        if(!initialRun)
            {
            if( count < 20)
                {
                count = count + 1
                if(getCurrentScore() == 0)
                    {
                    timer.invalidate()
                    ball.gameOver = true
                    gameOver = true
                    gameEnded()
                    }
                }
            else
                {
                count = 0
                timeLeft = timeLeft - 1
                let minutes = timeLeft / 60
                let seconds = timeLeft % 60
                if(seconds >= 10)
                    {
                    timeLabel.text = "Time: " + String(minutes) + ":" + String(seconds)
                    }
                else
                    {
                    timeLabel.text = "Time: " + String(minutes) + ":0" + String(seconds)
                    }
                if(timeLeft == 0)
                    {
                    let newScore = getCurrentScore()
                    score = score + newScore
                    timer.invalidate()
                    ball.gameOver = true
                    gameOver = true
                    gameEnded()
                    }
                else
                    {
                    let newScore = getCurrentScore()
                    score = score + newScore
                    scoreLabel.text = "Score: " + String(score)
                    if(newScore == 0)
                        {
                        timer.invalidate()
                        ball.gameOver = true
                        gameOver = true
                        gameEnded()
                        }
                    if(arc4random_uniform(8) == 2)
                        {
                        gameScheme.getWindDirection(boxSize: Double(width)/5.0)
                        if(gameScheme.windDidChange())
                            {
                            ball.addWind(x:gameScheme.xWind/Double(height),y: gameScheme.yWind/Double(height))
                            }
                        }
                    }
                }
            }
        else
            {
            runCount = runCount + 1
            if(runCount == 15)
                {
                initialRun = false
                controller.motionManager.startAccelerometerUpdates(to: controller.queue, withHandler: controller.update)
                }
            }
        }
//===============================================================================================================
//Handles user interaction with the game over screen
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
        {
         if(gameOver)
            {
            controller.motionManager.stopAccelerometerUpdates()
            if(controller.interstitial.isReady && !controller.adsDisabled)
                {
                controller.interstitial.present(fromRootViewController: controller)
                }
            controller.showMenu()
            }
        }
//===============================================================================================================
//Gets and updates the users score
    func getCurrentScore() -> Int
        {
        var calculatedScore = 0
        let pos = CGPoint(x: ball.xPos, y: ball.yPos)
        for i in 0 ... 4
            {
            if(tables[i].contains(pos) && calculatedScore == 0)
                {
                calculatedScore = scores[i]
                }
            }
        return calculatedScore
        }
//===============================================================================================================
//Adjusts the position of the ball in the game based on the accelerometer
    func moveBall(x: Double, y: Double)
        {
        ball.adjustPosition(xAccel: x, yAccel: y)
        ball.draw()
        }
//===============================================================================================================
//Displays a game over screen
    func gameEnded()
        {
        controller.motionManager.stopAccelerometerUpdates()
        if(score > controller.highscore)
            {
            controller.highscore = score
            controller.userDefaults.setValue(controller.highscore, forKey: "highscore")
            }
        let backgroundLabel = SKShapeNode(rect: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: center.x * 2, height: center.y * 2)))
        backgroundLabel.fillColor = UIColor(colorLiteralRed: 159.0/255.0, green: 200.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        addChild(backgroundLabel)
        let menuLabel = SKLabelNode(text:"Tap to return to menu")
        menuLabel.alpha = 0.35
        menuLabel.fontSize = CGFloat(frame.midX/6)
        menuLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.bottom
        menuLabel.fontColor = UIColor.black
        menuLabel.position = CGPoint(x: center.x, y: 0)
        addChild(menuLabel)
        let nameLabel = SKLabelNode(text:"Don't Fall Off")
        nameLabel.fontName = "AvenirNext-Bold"
        nameLabel.fontSize = CGFloat(frame.midX/3.5)
        nameLabel.fontColor = UIColor.black
        nameLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        nameLabel.position = CGPoint(x: center.x, y: center.y * 2)
        addChild(nameLabel)
        let nameLabel2 = SKLabelNode(text:"The Table")
        nameLabel2.fontName = "AvenirNext-Bold"
        nameLabel2.fontSize = CGFloat(frame.midX/3.5)
        nameLabel2.fontColor = UIColor.black
        nameLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        nameLabel2.position = CGPoint(x: center.x, y: nameLabel.position.y - (frame.midX/2))
        addChild(nameLabel2)
        let gameOverLabel = SKLabelNode(text:"Score: " + String(score))
        gameOverLabel.fontSize = CGFloat(frame.midX/4)
        gameOverLabel.fontColor = UIColor.black
        gameOverLabel.fontName = "AvenirNext-Bold"
        gameOverLabel.position = CGPoint(x: center.x, y: center.y)
        addChild(gameOverLabel)
        let highScoreLabel = SKLabelNode(text:"High score: " + String(controller.highscore))
        highScoreLabel.fontSize = CGFloat(frame.midX/6)
        highScoreLabel.fontColor = UIColor.black
        highScoreLabel.position = CGPoint(x: center.x, y: gameOverLabel.position.y - 50)
        addChild(highScoreLabel)
        }
    }
