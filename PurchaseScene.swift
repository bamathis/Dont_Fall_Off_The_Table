//
//  PurchaseScene.swift
//  Don't Fall Off The Table
//
//  Created by Brandon Mathis on 8/17/17.
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//

import UIKit
import SpriteKit
import StoreKit

class PurchaseScene: SKScene
{
    var removeAdButton: SKLabelNode!
    var addColorsButton: SKLabelNode!
    var addShapeButton: SKLabelNode!
    var controller: GameViewController!
    var restoreButton: SKLabelNode!
    var greenBackground: SKShapeNode!
 
//==============================================================================
//Sets up the purchase screen 
    override func didMove(to view: SKView)
        {
        backgroundColor = UIColor(colorLiteralRed: 159.0/255.0, green: 200.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        greenBackground = SKShapeNode(rect: CGRect(x: 10,y: frame.midY/1.5,width: frame.maxX - 20, height: frame.maxX/1.5), cornerRadius: CGFloat(10))
        greenBackground.fillColor = UIColor(colorLiteralRed: 133/255.0, green: 187.0/255.0, blue: 101/255.0, alpha: 1.0)
        greenBackground.strokeColor = UIColor.black
        addChild(greenBackground)
        let backgroundPicture = SKSpriteNode(imageNamed: "table.png")
        backgroundPicture.position = CGPoint(x: frame.midX, y: frame.midY)
        backgroundPicture.scale(to: CGSize(width: frame.maxX + 40, height: frame.maxX + 40))
        backgroundPicture.alpha = 0.2
        addChild(backgroundPicture)
        removeAdButton = SKLabelNode()
        removeAdButton.text = "Remove Ads"
        removeAdButton.fontSize = CGFloat(CGFloat(frame.midX/7))
        removeAdButton.fontName = "AvenirNext-Bold"
        if(controller.adsDisabled)
            {
            removeAdButton.fontColor = UIColor.darkGray
            }
        else
            {
            removeAdButton.fontColor = UIColor.white
            }
        removeAdButton.position = CGPoint(x: frame.midX, y: frame.maxY * 0.45)
        addChild(removeAdButton)
        addColorsButton = SKLabelNode()
        addColorsButton.text = "Purchase Color Pack"
        if(controller.hasColors)
            {
            addColorsButton.fontColor = UIColor.darkGray
            }
        else
            {
            addColorsButton.fontColor = UIColor.white
            }
        addColorsButton.fontSize = CGFloat(CGFloat(frame.midX/7))
        addColorsButton.position = CGPoint(x: frame.midX, y: frame.maxY * 0.55)
        addColorsButton.fontName = "AvenirNext-Bold"
        addChild(addColorsButton)
        addShapeButton = SKLabelNode()
        addShapeButton.text = "Purchase Table Pack"
        if(controller.hasShapes)
            {
            addShapeButton.fontColor = UIColor.darkGray
            }
        else
            {
            addShapeButton.fontColor = UIColor.white
            }
        addShapeButton.fontSize = CGFloat(CGFloat(frame.midX/7))
        addShapeButton.position = CGPoint(x: frame.midX, y: frame.maxY * 0.65)
        addShapeButton.fontName = "AvenirNext-Bold"
        addChild(addShapeButton)
        
        restoreButton = SKLabelNode()
        restoreButton.text = "Restore Purchases"
        if(controller.hasShapes && controller.hasColors && controller.adsDisabled)
            {
            restoreButton.fontColor = UIColor.darkGray
            }
        else
            {
            restoreButton.fontColor = UIColor.white
            }
        restoreButton.fontSize = CGFloat(CGFloat(frame.midX/7))
        restoreButton.position = CGPoint(x: frame.midX, y: frame.maxY * 0.35)
        restoreButton.fontName = "AvenirNext-Bold"
        addChild(restoreButton)
            
        let returnToMenuLabel = SKLabelNode(text:"Tap to return")
        returnToMenuLabel.alpha = 0.35
        returnToMenuLabel.fontSize = CGFloat(frame.midX/6)
        returnToMenuLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.bottom
        returnToMenuLabel.fontColor = UIColor.black
        returnToMenuLabel.position = CGPoint(x: frame.midX, y: 0)
        addChild(returnToMenuLabel)
        }
//==============================================================================
//Handles user interaction    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if let touch = touches.first
        {
            let pos = touch.location(in: self)
            let node = self.atPoint(pos)
            if (node == removeAdButton)
                {
                for product in controller.productsFromStore
                    {
                    let prodId = product.productIdentifier
                    if(prodId == "RemoveAds" && !controller.adsDisabled)
                        {
                        controller.purchaseProduct(product: product)
                        }
                    }
                }
            else if (node == addShapeButton)
                {
                for product in controller.productsFromStore
                    {
                    let prodId = product.productIdentifier
                    if(prodId == "TableShapePack" && !controller.hasShapes)
                       {
                        controller.purchaseProduct(product: product)
                        }
                    }
                }
            else if(node == addColorsButton)
                {
                for product in controller.productsFromStore
                    {
                    let prodId = product.productIdentifier
                    if(prodId == "TableColorPack" && !controller.hasColors)
                        {
                        controller.purchaseProduct(product: product)
                        }
                    
                    }
                }
            else if(node == restoreButton)
                {
                if(!controller.hasShapes || !controller.hasColors || !controller.adsDisabled)
                    {
                    SKPaymentQueue.default().restoreCompletedTransactions()
                    }
                }
            else
                {
                if(controller.previousScene == 0)
                    {
                    controller.showMenu()
                    }
                else if(controller.previousScene == 1)
                    {
                    controller.showColorSettings()
                    }
                else if(controller.previousScene == 2)
                    {
                        controller.showShapeSettings()
                    }
                }
        }
    }
}
