//
//  Wind.swift
//  Don't Fall Off The Table
//
//  Created by Brandon Mathis on 8/18/17.
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//

import Foundation
import SpriteKit

class Wind: SKShapeNode
    {
    init(size: Double, random: Int)
        {
        super.init()
        }
//==================================================================
//Updates the wind's direction and displays the direction to user
    func setWind(size: Double, random: Int)
        {
        let bezierPath = UIBezierPath()
        if(random == 0)
            {
            bezierPath.move(to: CGPoint(x: size/2, y: 0))
            bezierPath.addLine(to: CGPoint(x: size/2, y: size))
            bezierPath.addLine(to: CGPoint(x: 0, y: size/1.5))
            bezierPath.addLine(to: CGPoint(x: size/2, y: size))
            bezierPath.addLine(to: CGPoint(x: size, y: size/1.5))
            bezierPath.addLine(to: CGPoint(x: size/2, y: size))
            }
        else if(random == 1)
            {
            bezierPath.move(to: CGPoint(x: size/2, y: size))
            bezierPath.addLine(to: CGPoint(x: size/2, y: 0))
            bezierPath.addLine(to: CGPoint(x: 0, y: size * 0.35))
            bezierPath.addLine(to: CGPoint(x: size/2, y: 0))
            bezierPath.addLine(to: CGPoint(x: size, y: size * 0.35))
            bezierPath.addLine(to: CGPoint(x: size/2, y: 0))
            }
        else if(random == 2)
            {
            bezierPath.move(to: CGPoint(x: 0, y: size/2))
            bezierPath.addLine(to: CGPoint(x: size, y: size/2))
            bezierPath.addLine(to: CGPoint(x: size/1.5, y: 0))
            bezierPath.addLine(to: CGPoint(x: size, y: size/2))
            bezierPath.addLine(to: CGPoint(x: size/1.5, y: size))
            bezierPath.addLine(to: CGPoint(x: size, y: size/2))
            }
        else if(random == 3)
            {
            bezierPath.move(to: CGPoint(x: size, y: size/2))
            bezierPath.addLine(to: CGPoint(x: 0, y: size/2))
            bezierPath.addLine(to: CGPoint(x: size * 0.35, y: 0))
            bezierPath.addLine(to: CGPoint(x: 0, y: size/2))
            bezierPath.addLine(to: CGPoint(x: size * 0.35, y: size))
            bezierPath.addLine(to: CGPoint(x: 0, y: size/2))
            }
        else if(random == 4)
            {
            bezierPath.move(to: CGPoint(x: size, y: size))
            bezierPath.addLine(to: CGPoint(x: 0, y: 0))
            bezierPath.addLine(to: CGPoint(x: size * 0.15, y: size * 0.7))
            bezierPath.addLine(to: CGPoint(x: 0, y: 0))
            bezierPath.addLine(to: CGPoint(x: size * 0.75, y: size * 0.2))
            bezierPath.addLine(to: CGPoint(x: 0, y: 0))
            }
        else if(random == 5)
            {
            bezierPath.move(to: CGPoint(x: 0, y: 0))
            bezierPath.addLine(to: CGPoint(x: size, y: size))
            bezierPath.addLine(to: CGPoint(x: size * 0.25, y: size * 0.9))
            bezierPath.addLine(to: CGPoint(x: size, y: size))
            bezierPath.addLine(to: CGPoint(x: size * 0.95, y: size * 0.3))
            bezierPath.addLine(to: CGPoint(x: size, y: size))
            }
        else if(random == 6)
            {
            bezierPath.move(to: CGPoint(x: size, y: 0))
            bezierPath.addLine(to: CGPoint(x: 0, y: size))
            bezierPath.addLine(to: CGPoint(x: size * 0.65, y: size * 0.9))
            bezierPath.addLine(to: CGPoint(x: 0, y: size))
            bezierPath.addLine(to: CGPoint(x: size * 0.15, y: size * 0.25))
            bezierPath.addLine(to: CGPoint(x: 0, y: size))
            }
        else
            {
            bezierPath.move(to: CGPoint(x: 0, y: size))
            bezierPath.addLine(to: CGPoint(x: size, y: 0))
            bezierPath.addLine(to: CGPoint(x: size * 0.75, y: size * 0.9))
            bezierPath.addLine(to: CGPoint(x: size, y: 0))
            bezierPath.addLine(to: CGPoint(x: size * 0.20, y: size * 0.25))
            bezierPath.addLine(to: CGPoint(x: size, y: 0))
            }
        bezierPath.close()
        path = bezierPath.cgPath
        fillColor = UIColor.black
        strokeColor = UIColor.black
        lineWidth = CGFloat(6)
        }
//==================================================================
    required init?(coder aDecoder: NSCoder)
        {
        fatalError("init(coder:) has not been implemented")
        }
    }
