//
//  GameViewController.swift
//  Don't Fall Off The Table
//
//  Created by Brandon Mathis on 8/9/17.
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import CoreMotion
import StoreKit
import GoogleMobileAds

class GameViewController: UIViewController,SKPaymentTransactionObserver, SKProductsRequestDelegate, GADInterstitialDelegate
    {
    var gameScene: GameScene!
    var menuScene: MenuScene!
    var colorScene: TableColorScene!
    var shapeScene: TableShapeScene!
    var purchaseScene: PurchaseScene!
    var tutorialScene: TutorialScene!
    var motionManager: CMMotionManager!
    var interstitial: GADInterstitial!
    var appId = ""
    var unitId = ""
    var productID:NSSet!
    var productsRequest:SKProductsRequest!
    var productsFromStore = [SKProduct]()
    var highscore = 0
    var userDefaults = UserDefaults.standard
    var queue: OperationQueue!
    var adsDisabled = false
    var hasShapes = false
    var hasColors = false
    var gameScheme: GameScheme!
    var colorSelected = 0
    var shapeSelected = 0
    var previousScene = -1

//=====================================================================
//Sets up Google Mobile Ads and loads initial screen
    override func viewDidLoad()
        {
        super.viewDidLoad()
        UIApplication.shared.isIdleTimerDisabled = true
        gameScheme = GameScheme(colorIn: 0, shapeIn: 0)
        GADMobileAds.configure(withApplicationID: appId)
        createAndLoadInterstitial()
        if(SKPaymentQueue.canMakePayments())
            {
            productID = NSSet(objects: "RemoveAds","TableColorPack","TableShapePack")
            productsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>)
            productsRequest.delegate = self
            productsRequest.start()
            SKPaymentQueue.default().add(self)
            }
        if (userDefaults.value(forKey: "adsDisabled") != nil)
            {
            adsDisabled = userDefaults.value(forKey: "adsDisabled") as! Bool
            }
        if (userDefaults.value(forKey: "hasShapes") != nil)
            {
            hasShapes = userDefaults.value(forKey: "hasShapes") as! Bool
            }
        if (userDefaults.value(forKey: "hasColors") != nil)
            {
            hasColors = userDefaults.value(forKey: "hasColors") as! Bool
            }
        if (userDefaults.value(forKey: "highscore") != nil)
            {
            highscore = userDefaults.value(forKey: "highscore") as! Int
            showMenu()
            }
        else
            {
            highscore = -1
            showTutorial()
            }
        }
//======================================================================
    override func didReceiveMemoryWarning()
        {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
        }
//=======================================================================
//Hides the Status Bar
    override var prefersStatusBarHidden: Bool
        {
        return true
        }
//=======================================================================
//Creates a Google Mobile Ad request
    func createAndLoadInterstitial()
        {
        interstitial = GADInterstitial(adUnitID: unitId)
        interstitial.delegate = self
        interstitial.load(GADRequest())
        }
//=======================================================================
//Calls createAndLoadInterstitial to create a Google Mobile Ad request
    func interstitialDidDismissScreen(_ ad: GADInterstitial)
        {
        createAndLoadInterstitial()
        }
//=====================================================================
//Presents the Menu Screen
    func showMenu()
        {
        previousScene = 0
        let skView = view as! SKView
        menuScene = MenuScene(size: skView.bounds.size)
        menuScene.controller = self
        menuScene.scaleMode = .aspectFill
        skView.presentScene(menuScene)
        }
//=======================================================================
//Presents the Game Screen
    func startGame()
        {
        gameScheme.setScheme(colorIn: colorSelected, shapeIn: shapeSelected)
        motionManager = CMMotionManager()
        queue = OperationQueue()
        let skView = view as! SKView
        gameScene = GameScene(size: skView.bounds.size)
        gameScene.scaleMode = .aspectFill
        gameScene.controller = self
        gameScene.gameScheme = gameScheme
        gameScene.drawGame(view:skView)
        skView.presentScene(gameScene,transition: SKTransition.crossFade(withDuration: 1))
        }
//=======================================================================
//Presents the Color Select Screen
    func showColorSettings()
        {
        previousScene = 1
        let skView = view as! SKView
        colorScene = TableColorScene(size: skView.bounds.size)
        colorScene.controller = self
        colorScene.scaleMode = .aspectFill
        colorScene.setupDisplay(to: skView)
        skView.presentScene(colorScene)
        }
//=======================================================================
//Presents the Shape Select Screen
    func showShapeSettings()
        {
        previousScene = 2
        let skView = view as! SKView
        shapeScene = TableShapeScene(size: skView.bounds.size)
        shapeScene.controller = self
        shapeScene.scaleMode = .aspectFill
        shapeScene.setupDisplay(to: skView)
        skView.presentScene(shapeScene)
        }
//=====================================================================
//Presents the Tutorial Screen
    func showTutorial()
        {
        let skView = view as! SKView
        tutorialScene = TutorialScene(size: skView.bounds.size)
        tutorialScene.controller = self
        tutorialScene.scaleMode = .aspectFill
        tutorialScene.setupDisplay(to: skView)
        skView.presentScene(tutorialScene)
        }
//=====================================================================
//Presents the Purchase Screen
    func showPurchases()
        {
        let skView = view as! SKView
        purchaseScene = PurchaseScene(size: skView.bounds.size)
        purchaseScene.controller = self
        purchaseScene.scaleMode = .aspectFill
        skView.presentScene(purchaseScene)
        }
//=======================================================================
//Makes a list of products that were requested for purchase
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse)
        {
        let products = response.products
        for product in products
            {
            productsFromStore.append(product)
            }
        }
//=======================================================================
//Purchases the product that was passed if payment is enabled
    func purchaseProduct(product: SKProduct)
        {
        if (SKPaymentQueue.canMakePayments())
            {
            SKPaymentQueue.default().add(SKPayment(product: product))
            }
        else
            {
            print("Payment disabled")
            }
        }
//======================================================================
//Restores previously purchased items
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue)
        {
        for transaction in queue.transactions
            {
            let prodID = transaction.payment.productIdentifier as String
            switch prodID
                {
                case "RemoveAds":
                    adsDisabled = true
                    userDefaults.setValue(adsDisabled, forKey: "adsDisabled")
                    break
                case "TableColorPack":
                    hasColors = true
                    userDefaults.setValue(hasColors, forKey: "hasColors")
                    break
                case "TableShapePack":
                    hasShapes = true
                    userDefaults.setValue(hasShapes, forKey: "hasShapes")
                    break
                default:
                    print("Item not found")
                }
            }
        }
//======================================================================
//Completes purchase of the requested products
     func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction])
        {
            for transaction in transactions
            {
                switch transaction.transactionState
                {
                case .purchased:
                    let prodID = transaction.payment.productIdentifier as String
                    switch prodID
                        {
                    case "RemoveAds":
                        adsDisabled = true
                        userDefaults.setValue(adsDisabled, forKey: "adsDisabled")
                        break
                    case "TableColorPack":
                        hasColors = true
                        userDefaults.setValue(hasColors, forKey: "hasColors")
                        break
                    case "TableShapePack":
                        hasShapes = true
                        userDefaults.setValue(hasShapes, forKey: "hasShapes")
                        break
                    default:
                        print("Item not found")
                        }
                    queue.finishTransaction(transaction)
                    break
                case .restored:
                    let prodID = transaction.payment.productIdentifier as String
                    switch prodID
                        {
                    case "RemoveAds":
                        adsDisabled = true
                        userDefaults.setValue(adsDisabled, forKey: "adsDisabled")
                        break
                    case "TableColorPack":
                        hasColors = true
                        userDefaults.setValue(hasColors, forKey: "hasColors")
                        break
                    case "TableShapePack":
                        hasShapes = true
                        userDefaults.setValue(hasShapes, forKey: "hasShapes")
                        break
                    default:
                        break
                        }
                    queue.finishTransaction(transaction)
                    break
                case .failed:
                        queue.finishTransaction(transaction)
                        break
                default:
                    break
                }
            }
        }
//======================================================================
//Updates the ball position from the accelerometer data
    func update(data: CMAccelerometerData? , error: Error?) -> Void
        {
        if let accelData = data
        {
        gameScene.moveBall(x: accelData.acceleration.x, y: accelData.acceleration.y)
        }
        }
    }
