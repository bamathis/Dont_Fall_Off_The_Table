//
//  Table.swift
//  Don't Fall Off The Table
//
//  Created by Brandon Mathis on 8/9/17.
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//

import Foundation
import SpriteKit

class Table: SKShapeNode
    {
 
//===========================================================================
//Initializes table as a circle
    init(circleOfRadius: CGFloat)
        {
        super.init()
        let size = circleOfRadius * 2
        path = CGPath(ellipseIn: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: size, height: size )), transform: nil)
        }
//===========================================================================
//Initializes table as a square
    init(squareInRadius: CGFloat)
        {
        super.init()
        let size = squareInRadius * 2
        path = CGPath(rect: CGRect(x: 0, y: 0, width: size, height: size), transform: nil)
        }
//===========================================================================
//Initializes table as a triangle
    init(triangleInRadius: CGFloat,up: Int)
        {
        super.init()
        let size = triangleInRadius * 2
        let bezierPath = UIBezierPath()
        if(up == 0)
            {
            bezierPath.move(to: CGPoint(x: size/2, y: size))
            bezierPath.addLine(to: CGPoint(x: size, y: 0))
            bezierPath.addLine(to: CGPoint(x: 0, y: 0))
            }
        else
            {
            bezierPath.move(to: CGPoint(x: size/2, y: 0))
            bezierPath.addLine(to: CGPoint(x: size, y: size))
            bezierPath.addLine(to: CGPoint(x: 0, y: size))
            }
        bezierPath.close()
        path = bezierPath.cgPath
        }
//===========================================================================
//Initializes table as a heart
    init(heartInRadius: CGFloat)
        {
        super.init()
        let size = heartInRadius * 2
        let bezierPath = UIBezierPath()
            
        bezierPath.move(to: CGPoint(x: size/2, y: 0))
        bezierPath.addLine(to: CGPoint(x: size * 0.95 , y: size * 0.5))
        bezierPath.addArc(withCenter: CGPoint(x: size * 0.75, y: size * 0.65), radius: size * 0.25, startAngle: -.pi/4, endAngle: .pi, clockwise: true)
        bezierPath.addArc(withCenter: CGPoint(x: size * 0.25, y: size * 0.65), radius: size * 0.25, startAngle: 0, endAngle: -.pi * 0.75, clockwise: true)
        bezierPath.close()
        path = bezierPath.cgPath
        }
//===========================================================================
//Initializes table as an L
    init(lInRadius: CGFloat,up: Int)
        {
        super.init()
        let size = lInRadius * 2
        let bezierPath = UIBezierPath()
        if(up == 0)
            {
            bezierPath.move(to: CGPoint(x: size/4, y: size))
            bezierPath.addLine(to: CGPoint(x: size/4, y: 0))
            bezierPath.addLine(to: CGPoint(x: size - size/8, y: 0))
            bezierPath.addLine(to: CGPoint(x: size - size/8, y: size/2))
            bezierPath.addLine(to: CGPoint(x: size/1.65, y: size/2))
            bezierPath.addLine(to: CGPoint(x: size/1.65, y: size))
            }
        else
            {
            bezierPath.move(to: CGPoint(x: size * 0.75, y: size))
            bezierPath.addLine(to: CGPoint(x: size * 0.75, y: 0))
            bezierPath.addLine(to: CGPoint(x: size/8, y: 0))
            bezierPath.addLine(to: CGPoint(x: size/8, y: size/2))
            bezierPath.addLine(to: CGPoint(x: size - size/1.65, y: size/2))
            bezierPath.addLine(to: CGPoint(x: size - size/1.65, y: size))
            }
        bezierPath.close()
        path = bezierPath.cgPath
        }
//===========================================================================
//Initializes table as a Z
    init(zInRadius: CGFloat)
    {
        super.init()
        let size = zInRadius * 2
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: size * 0.75, y: size * 0.25))
        bezierPath.addLine(to: CGPoint(x: size * 0.5 , y: size * 0.25))
        bezierPath.addLine(to: CGPoint(x: size , y: size))
        bezierPath.addLine(to: CGPoint(x: 0 , y: size))
        bezierPath.addLine(to: CGPoint(x: 0 , y: size * 0.75))
        bezierPath.addLine(to: CGPoint(x: size * 0.5 , y: size * 0.75))
        bezierPath.addLine(to: CGPoint(x: 0 , y: 0))
        bezierPath.addLine(to: CGPoint(x: size , y: 0))
        bezierPath.addLine(to: CGPoint(x: size , y: size * 0.25))
        bezierPath.close()
        path = bezierPath.cgPath
    }
//===========================================================================
//Initializes table as a T
    init(tInRadius: CGFloat)
        {
        super.init()
        let size = tInRadius * 2
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 0, y: size))
        bezierPath.addLine(to: CGPoint(x: size, y: size))
        bezierPath.addLine(to: CGPoint(x: size, y: size * 0.75))
        bezierPath.addLine(to: CGPoint(x: size * 0.75, y: size * 0.75))
        bezierPath.addLine(to: CGPoint(x: size * 0.75, y: 0))
        bezierPath.addLine(to: CGPoint(x: size/4, y: 0))
        bezierPath.addLine(to: CGPoint(x: size/4, y: size * 0.75))
        bezierPath.addLine(to: CGPoint(x: 0, y: size * 0.75))
        bezierPath.close()
        path = bezierPath.cgPath
        }
//===========================================================================
//Initializes table as a half circle
    init(halfCircleInRadius: CGFloat, up: Int)
    {
        super.init()
        let size = halfCircleInRadius * 2
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: size, y: size/2))
        if(up == 0)
            {
            bezierPath.addArc(withCenter: CGPoint(x: size/2, y: size/2), radius: size/2, startAngle: 0, endAngle: .pi, clockwise: true)
            bezierPath.addLine(to: CGPoint(x: 0 , y: size/2.1))
            bezierPath.addLine(to: CGPoint(x: size , y: size/2.1))
            }
        else
            {
            bezierPath.addArc(withCenter: CGPoint(x: size/2, y: size/2), radius: size/2, startAngle: 0, endAngle: .pi, clockwise: false)
            bezierPath.addLine(to: CGPoint(x: 0 , y: size/1.9))
            bezierPath.addLine(to: CGPoint(x: size , y: size/1.9))
            }
        bezierPath.close()
        path = bezierPath.cgPath
    }
//===========================================================================
    required init?(coder aDecoder: NSCoder)
        {
        fatalError("init(coder:) has not been implemented")
        }
    }
