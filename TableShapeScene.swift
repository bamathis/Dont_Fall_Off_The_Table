//
//  TableShapeScene.swift
//  Don't Fall Off The Table
//
//  Created by Brandon Mathis on 8/18/17.
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//

import SpriteKit

class TableShapeScene: SKScene
{
    var buttons = [SKLabelNode]()
    var returnButton: SKLabelNode!
    var previousButton: SKLabelNode!
    var nextButton: SKLabelNode!
    var controller: GameViewController!
    var yPos = CGFloat(0)
    var selectedLabel: SKLabelNode!
    override func didMove(to view: SKView)
        {
        
    	}
//==============================================================================
//Sets up the select screen for table shapes
    func setupDisplay(to view: SKView)
        {
        backgroundColor = UIColor(colorLiteralRed: 159.0/255.0, green: 200.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        let backgroundPicture = SKSpriteNode(imageNamed: "table.png")
        backgroundPicture.position = CGPoint(x: frame.midX, y: frame.midY)
        backgroundPicture.scale(to: CGSize(width: frame.maxX + 40, height: frame.maxX + 40))
        backgroundPicture.alpha = 0.1
        addChild(backgroundPicture)
        let shapeLabel = SKLabelNode()
        shapeLabel.text = "Table Shapes"
        shapeLabel.fontSize = CGFloat(frame.midX/5)
        shapeLabel.fontName = "AvenirNext-Bold"
        shapeLabel.fontColor = UIColor.black
        shapeLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        shapeLabel.position = CGPoint(x: frame.midX, y: frame.maxY)
        addChild(shapeLabel)
        yPos = frame.maxY - CGFloat(100)
        var firstPass = true
        let list = controller.gameScheme.getShapeList()
        for shape in list
            {
            let button = SKLabelNode()
            button.text = shape
            button.fontSize = CGFloat(frame.midX/7)
            button.fontName = "AvenirNext-Bold"
            if(controller.hasShapes || firstPass)
                {
                button.fontColor = UIColor.black
                firstPass = false
                }
            else
                {
                button.fontColor = UIColor.lightGray
                }
            button.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
            button.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
            button.position = CGPoint(x: 0, y: yPos)
            addChild(button)
            buttons.append(button)
            yPos = yPos - CGFloat(Double(frame.midY)/(Double(list.count)/1.5))
            }
        
        selectedLabel = SKLabelNode()
        selectedLabel.text = "X"
        selectedLabel.fontSize = CGFloat(frame.midX/7)
        selectedLabel.fontName = "AvenirNext-Bold"
        selectedLabel.fontColor = UIColor.black
        selectedLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        selectedLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.right
        selectedLabel.position = CGPoint(x: frame.maxX, y: buttons[controller.shapeSelected].position.y)
        addChild(selectedLabel)
        
        
        previousButton = SKLabelNode()
        previousButton.text = "<< Colors"
        previousButton.fontSize = CGFloat(frame.midX/8)
        previousButton.fontName = "AvenirNext-Bold"
        previousButton.fontColor = UIColor.black
        previousButton.verticalAlignmentMode = SKLabelVerticalAlignmentMode.bottom
        previousButton.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        previousButton.position = CGPoint(x: frame.minX, y: 10)
        addChild(previousButton)
        
        returnButton = SKLabelNode()
        returnButton.text = "Menu"
        returnButton.fontSize = CGFloat(frame.midX/8)
        returnButton.fontName = "AvenirNext-Bold"
        returnButton.fontColor = UIColor.black
        returnButton.verticalAlignmentMode = SKLabelVerticalAlignmentMode.bottom
        returnButton.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        returnButton.position = CGPoint(x: frame.midX, y: 10)
        addChild(returnButton)
        
        nextButton = SKLabelNode()
        nextButton.text = "Tutorial >> "
        nextButton.fontSize = CGFloat(frame.midX/8)
        nextButton.fontName = "AvenirNext-Bold"
        nextButton.fontColor = UIColor.black
        nextButton.verticalAlignmentMode = SKLabelVerticalAlignmentMode.bottom
        nextButton.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.right
        nextButton.position = CGPoint(x: frame.maxX, y: 5)
        addChild(nextButton)
        }
//==============================================================================
//Handles user interaction
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
        {
        if let touch = touches.first
            {
            let pos = touch.location(in: self)
            let node = self.atPoint(pos)
            if(node == previousButton)
                {
                controller.showColorSettings()
                }
            else if(node == returnButton)
                {
                controller.showMenu()
                }
            else if(node == nextButton)
                {
                controller.showTutorial()
                }
            else
                {
                var found = false
                var i = 0
                for button in buttons
                    {
                    if(button == node)
                        {
                        found = true
                        if(controller.hasShapes)
                            {
                            controller.shapeSelected = i
                            selectedLabel.position = CGPoint(x: frame.maxX, y: buttons[controller.shapeSelected].position.y)
                            }
                        }
                    i = i + 1
                    }
                if(found && node != buttons.first && !controller.hasShapes)
                    {
                    controller.showPurchases()
                    }
                }
            }
        }
    }
