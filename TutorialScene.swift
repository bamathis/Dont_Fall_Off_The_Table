//
//  TutorialScene.swift
//  Don't Fall Off The Table
//
//  Created by Brandon Mathis on 8/19/17.
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//

import Foundation
import SpriteKit

class TutorialScene: SKScene
    {
    var text:SKLabelNode!
    var returnButton: SKLabelNode!
    var previousButton: SKLabelNode!
    var controller: GameViewController!
    var yPos = CGFloat(0)
    override func didMove(to view: SKView)
        {
        
        }
//==============================================================================
//Sets up the tutorial screen
    func setupDisplay(to view: SKView)
        {
        backgroundColor = UIColor(colorLiteralRed: 159.0/255.0, green: 200.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        let backgroundPicture = SKSpriteNode(imageNamed: "table.png")
        backgroundPicture.position = CGPoint(x: frame.midX, y: frame.midY)
        backgroundPicture.scale(to: CGSize(width: frame.maxX + 40, height: frame.maxX + 40))
        backgroundPicture.alpha = 0.2
        addChild(backgroundPicture)

        let tutorialLabel = SKLabelNode()
        tutorialLabel.text = "Tutorial"
        tutorialLabel.fontSize = CGFloat(frame.midX/5)
        tutorialLabel.fontName = "AvenirNext-Bold"
        tutorialLabel.fontColor = UIColor.black
        tutorialLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        tutorialLabel.position = CGPoint(x: frame.midX, y: frame.maxY)
        addChild(tutorialLabel)
        var yPos = frame.midY + (frame.midY/5)
        
        text = SKLabelNode()
        text.text = "Tilt your phone to rack up"
        text.fontSize = CGFloat(frame.midX/8)
        text.fontName = "AvenirNext-Bold"
        text.fontColor = UIColor.black
        text.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        text.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        text.position = CGPoint(x: frame.midX, y: yPos)
        addChild(text)
           
        yPos = yPos - CGFloat(Double(frame.midY)/9)
        text = SKLabelNode()
        text.text = "as many points as you can in"
        text.fontSize = CGFloat(frame.midX/8)
        text.fontName = "AvenirNext-Bold"
        text.fontColor = UIColor.black
        text.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        text.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        text.position = CGPoint(x: frame.midX, y: yPos)
        addChild(text)
            
        yPos = yPos - CGFloat(Double(frame.midY)/9)
        text = SKLabelNode()
        text.text = "the given time, while fighting"
        text.fontSize = CGFloat(frame.midX/8)
        text.fontName = "AvenirNext-Bold"
        text.fontColor = UIColor.black
        text.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        text.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        text.position = CGPoint(x: frame.midX, y: yPos)
        addChild(text)
            
        yPos = yPos - CGFloat(Double(frame.midY)/9)
        text = SKLabelNode()
        text.text = "gravity as well as some"
        text.fontSize = CGFloat(frame.midX/8)
        text.fontName = "AvenirNext-Bold"
        text.fontColor = UIColor.black
        text.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        text.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        text.position = CGPoint(x: frame.midX, y: yPos)
        addChild(text)
            
        yPos = yPos - CGFloat(Double(frame.midY)/9)
        text = SKLabelNode()
        text.text = "very obnoxious winds."
        text.fontSize = CGFloat(frame.midX/8)
        text.fontName = "AvenirNext-Bold"
        text.fontColor = UIColor.black
        text.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        text.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        text.position = CGPoint(x: frame.midX, y: yPos)
        addChild(text)
            
        if(controller.previousScene != -1)		//if the tutorial was accessed from the settings page
            {
            previousButton = SKLabelNode()
            previousButton.text = "<< Shapes "
            previousButton.fontSize = CGFloat(frame.midX/8)
            previousButton.fontName = "AvenirNext-Bold"
            previousButton.fontColor = UIColor.black
            previousButton.verticalAlignmentMode = SKLabelVerticalAlignmentMode.bottom
            previousButton.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
            previousButton.position = CGPoint(x: frame.minX, y: 5)
            addChild(previousButton)
    
            returnButton = SKLabelNode()
            returnButton.text = "Menu"
            returnButton.fontSize = CGFloat(frame.midX/8)
            returnButton.fontName = "AvenirNext-Bold"
            returnButton.fontColor = UIColor.black
            returnButton.verticalAlignmentMode = SKLabelVerticalAlignmentMode.bottom
            returnButton.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
            returnButton.position = CGPoint(x: frame.midX, y: 10)
            addChild(returnButton)
            }
        else
            {
            let returnToMenuLabel = SKLabelNode(text:"Tap to go to menu")
            returnToMenuLabel.alpha = 0.35
            returnToMenuLabel.fontSize = CGFloat(frame.midX/6)
            returnToMenuLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.bottom
            returnToMenuLabel.fontColor = UIColor.black
            returnToMenuLabel.position = CGPoint(x: frame.midX, y: 0)
            addChild(returnToMenuLabel)
            }
    }
//==============================================================================
//Handles user interaction
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
        {
        if let touch = touches.first
            {
            if(controller.previousScene == -1)
                {
                controller.showMenu()
                }
            else
                {
                let pos = touch.location(in: self)
                let node = self.atPoint(pos)
                if(node == previousButton)
                    {
                    controller.showShapeSettings()
                    }
                else if(node == returnButton)
                    {
                    controller.showMenu()
                    }
                }
            
            }
        }
    }
