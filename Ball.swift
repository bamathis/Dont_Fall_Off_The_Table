//
//  Ball.swift
//  Don't Fall Off The Table
//
//  Created by Brandon Mathis on 8/9/17.
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//

import Foundation
import SpriteKit

class Ball: SKShapeNode
{
    var xPos = 0.0
    var yPos = 0.0
    var xMotionScale = 0.06
    var yMotionScale = 0.1
    var diameter: CGFloat = 0.0
    var xMotion = 0.0
    var yMotion = 0.0
    var gameOver = false
    var xWind = 0.0
    var yWind = 0.0
    var prevXWind = 0.0
    var prevYWind = 0.0
    
//================================================================================
//Adjusts the speed and direction of motion
    func adjustPosition(xAccel: Double, yAccel: Double)
        {
        if(!gameOver)
            {
            xMotion += xAccel * xMotionScale + xWind
            yMotion += yAccel * yMotionScale + yWind
            xPos = xPos + xMotion
            yPos = yPos + yMotion
            }
        }
//===============================================================================
//Changes the wind effect
    func addWind(x: Double, y: Double)
        {
        xWind = x
        yWind = y
        }
//===============================================================================
//Sets the size of the ball
    func setDiameter(circleOfRadius: CGFloat)
        {
        diameter = circleOfRadius * 4
        }
//===============================================================================
//Updates the ball's position
    func draw()
        {
        position = CGPoint(x:xPos, y: yPos)
        }
}
