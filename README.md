# Don't_Fall_Off_The_Table

[Don't_Fall_Off_The_Table](https://gitlab.com/bamathis/Dont_Fall_Off_The_Table) is a accelerometer based iOS game developed using Swift.

### Dependencies

- Swift 3.1+
- Google Mobile Ads SDK 7.31.0

### Download On the App Store
https://itunes.apple.com/us/app/dont-fall-off-the-table/id1272454928?mt=8
